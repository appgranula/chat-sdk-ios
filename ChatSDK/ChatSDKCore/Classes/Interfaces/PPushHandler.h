//
//  BPushHandler.h
//  Pods
//
//  Created by Benjamin Smiley-andrews on 31/03/2016.
//
//

#ifndef PPushHandler_h
#define PPushHandler_h

#import <ChatSDKCore/PMessage.h>
/*
 public static final String ACTION = "action";
 public static final String ALERT = "alert";
 public static final String BADGE = "badge", INCREMENT = "Increment";
 public static final String CONTENT = "text";
 public static final String MESSAGE_ENTITY_ID = "entity_id";
 public static final String THREAD_ENTITY_ID = "thread_entity_id";
 public static final String MESSAGE_DATE ="date";
 public static final String MESSAGE_SENDER_ENTITY_ID ="sender_entity_id";
 public static final String MESSAGE_SENDER_NAME ="sender_name";
 public static final String MESSAGE_TYPE = "type";
 public static final String MESSAGE_PAYLOAD= "payload";
 
 
 */
// Setup some defines for push notifications
#define bAction @"action"
#define bAlert @"alert" //@"ios-alert"
#define bContent @"text" // @"content"
#define bMessageEntityID @"entity_id"// @"message_entity_id"
#define bThreadEntityID @"thread_entity_id"
#define bMessageDate @"date" //@"message_date"
#define bMessageSenderEntityID @"sender_entity_id"// @"message_sender_entity_id"
#define bMessage_Type @"type"//@"message_type"
#define bMessagePayload @"payload"//@"message_payload"
#define bBadge @"badge"//@"ios-badge"
#define bIncrement @"Increment"
#define bIOSSound @"ios-sound"
#define bDefault @"default"

@protocol PPushHandler <NSObject>

- (void) application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;

- (void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo;

-(void) registerForPushNotificationsWithApplication: (UIApplication *) app launchOptions: (NSDictionary *) options;

-(void) subscribeToPushChannel: (NSString *) channel;
-(void) unsubscribeToPushChannel: (NSString *) channel;

-(void) pushToChannels: (NSArray *) channels withData:(NSDictionary *) data;
-(void) pushForMessage: (id<PMessage>) message;

@end

#endif /* PPushHandler_h */
