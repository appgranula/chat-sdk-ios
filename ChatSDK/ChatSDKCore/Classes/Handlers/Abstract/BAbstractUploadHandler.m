//
//  BAbstractUploadHandler.m
//  Pods
//
//  Created by Benjamin Smiley-andrews on 12/11/2016.
//
//

#import "BAbstractUploadHandler.h"
#import <ChatSDKCore/ChatCore.h>

@implementation BAbstractUploadHandler

-(RXPromise *) uploadImage:(UIImage *)image thumbnail: (UIImage *) thumbnail {
    
    // Upload the images:
    return [RXPromise all:@[[self uploadFile:UIImageJPEGRepresentation(image, 0.6f) withName:@"image.jpg" mimeType:@"image/jpeg"],
                            [self uploadFile:UIImageJPEGRepresentation(image, 0.6f) withName:@"thumbnail.jpg" mimeType:@"image/jpeg"]]].thenOnMain(^id(NSArray * results) {
        NSMutableDictionary * urls = [NSMutableDictionary new];
        for (NSDictionary * result in results) {
            if ([result[bFileName] hasSuffix:@"image.jpg"]) {
                urls[bImagePath] = result[bFilePath];
            }
            if ([result[bFileName] hasSuffix:@"thumbnail.jpg"]) {
                urls[bThumbnailPath] = result[bFilePath];
            }
        }
        return urls;
    }, ^id(NSError * error) {
        NSLog(@"%@", error);
        return error;
    });
}

-(RXPromise *) uploadImage:(UIImage *)image maxBorder: (CGFloat) border{
    CGSize newSize;
    UIImage* resizedImage;
    if (image.size.width > image.size.height) {
        newSize = CGSizeMake(border, border * image.size.height/image.size.width);
    }
    else {
        newSize = CGSizeMake(border * image.size.width/image.size.height, border);
    }
    resizedImage = [image resizedImage:newSize interpolationQuality:kCGInterpolationHigh];
    
    return [self uploadFile:UIImageJPEGRepresentation(resizedImage, 1.0f) withName:@"image.jpg" mimeType:@"image/jpeg"].thenOnMain(^id(NSDictionary * result) {
        NSString* url;
        if ([result[bFileName] hasSuffix:@"image.jpg"]) {
                url = result[bFilePath];
        }
        return url;
    }, ^id(NSError * error) {
        NSLog(@"%@", error);
        return error;
    });
}

-(RXPromise *) uploadFile:(NSData *)file withName: (NSString *) name mimeType: (NSString *) mimeType {
    assert(NO);
}

@end
