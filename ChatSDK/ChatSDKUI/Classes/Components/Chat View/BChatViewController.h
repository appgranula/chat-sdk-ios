//
//  BChatViewController.h
//  Pods
//
//  Created by Benjamin Smiley-andrews on 02/02/2017.
//
//

#import <Foundation/Foundation.h>
#import <ChatSDKUI/ElmChatViewController.h>
#import <ChatSDKUI/ElmChatViewDelegate.h>

@protocol PThread;

@interface BChatViewController : ElmChatViewController<ElmChatViewDelegate> {
    
    id _messageObserver;
    id _userObserver;
    id _typingObserver;
    id _readReceiptObserver;
    id _threadUsersObserver;
    
    BOOL _usersViewLoaded;
    
    NSMutableArray * _messageCache;
    BOOL _messageCacheDirty;
    
}

@property (strong, nonatomic) id<PThread> thread;

- (id)initWithThread: (id<PThread>) thread;

@end
