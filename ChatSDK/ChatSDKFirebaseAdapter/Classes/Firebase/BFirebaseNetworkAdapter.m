//
//  BFirebaseNetworkAdapter.m
//  Pods
//
//  Created by Benjamin Smiley-andrews on 13/11/2016.
//
//

#import "BFirebaseNetworkAdapter.h"

#import <ChatSDKCore/ChatCore.h>
#import <ChatSDKFirebaseAdapter/ChatFirebaseAdapter.h>

@implementation BFirebaseNetworkAdapter

-(id) init {
    if((self = [super init])) {
        
        // Configure app for Facebook login
        [FIRApp configure];
        
        [[FIRConfiguration sharedInstance] setLoggerLevel:FIRLoggerLevelWarning];
        
        
        FIRDatabaseReference *connectedRef = [[FIRDatabase database] referenceWithPath:@".info/connected"];
        [connectedRef observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *snapshot) {
            if([snapshot.value boolValue]) {
                NSLog(@"!!connected");
            } else {
                NSLog(@"!!not connected");
            }
        }];
        
        self.core = [[BFirebaseCoreHandler alloc] init];
        self.upload = [[BFirebaseUploadHandler alloc] init];
        self.auth = [[BFirebaseAuthenticationHandler alloc] init];
        self.search = [[BFirebaseSearchHandler alloc] init];
        self.moderation = [[BFirebaseModerationHandler alloc] init];
        self.contact = [[BBaseContactHandler alloc] init];
        self.publicThread = [[BFirebasePublicThreadHandler alloc] init];
        
        // DISABLE?!
       // self.push = [BFirebasePushHandler new];
    
    
    }
    return self;
}

@end
