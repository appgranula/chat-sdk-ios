//
//  BFirebasePushHandler.m
//  Pods
//
//  Created by Юджин Топсекретович on 6/23/17.
//
//

#import "BFirebasePushHandler.h"
#import "AFNetworking.h"

#define postUrl @"https://fcm.googleapis.com/fcm/send"

@interface BFirebasePushHandler()
@property (strong, nonatomic) AFHTTPSessionManager *manager;
@end

@implementation BFirebasePushHandler


-(instancetype)init{
    self = [super init];
    if (self){
        _manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        _manager.requestSerializer = [AFJSONRequestSerializer serializer];
        _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [_manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [_manager.requestSerializer setValue:@"key=AIzaSyCKxrF4lVqBV4wgi2zOvUXfOrWkoLxB1Jg" forHTTPHeaderField:@"Authorization"];

        

    }
    return self;
}


- (void) application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
   
}

- (void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
  
}

-(void) registerForPushNotificationsWithApplication: (UIApplication *) app launchOptions: (NSDictionary *) options {

}

-(void) subscribeToPushChannel: (NSString *) channel {
    
}

-(void) unsubscribeToPushChannel: (NSString *) channel {
    
}


-(void) pushByTokens: (NSArray *) tokens withData:(NSDictionary *) data {
    
   // NSMutableArray* promises = [NSMutableArray array];
    for (NSString* token in tokens) {
        
        NSDictionary* dict = @{@"to": token, @"data": data, @"notification": @{@"title": data[bAlert], @"body": data[bMessagePayload] }};
        NSLog(@"%@", dict);
        [_manager POST:postUrl parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"success!");
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"error: %@", error);
            NSString* errResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
            NSLog(@"%@",errResponse);
        }];
    }
    
//    [_manager POST:postUrl parameters:data progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        NSLog(@"success!");
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        NSLog(@"error: %@", error);
//    }];
}


-(void) pushToChannels: (NSArray *) channels withData:(NSDictionary *) data {

}

@end
